;;; amigaguide-mode.el --- Major mode for browsing AmigaGuide databases -*- lexical-binding: t -*-

;; Author: marco rolappe
;; Maintainer: marco rolappe
;; Version: 0.1
;; Package-Requires: (TODO)
;; Homepage: https://codeberg.org/mrolappe/amigaguide-mode/
;; Keywords: amiga,amigaguide


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Major mode for browsing AmigaGuide databases


;;; Code:

;; TODO bookmark handler
;; TODO imenu support
;; TODO selection: mark node(s), mark link(s)
;; TODO navigation: main, toc, index, next/prev node, first/last node
;; TODO? export: org, html
;; TODO? check links

(defface amigaguide-link
    '((t :inherit link))
    "Face for unvisited links"
    :group 'amigaguide)

(defface amigaguide-link-visited
    '((t :inherit (link-visited amigaguide-link)))
    "Face for visited links"
    :group 'amigaguide)

(defvar-local amigaguide-current-node nil
    "Name of the node currently being visited or nil")

(defun amigaguide-mode/goto-next-node ()
    "Go to the next node of the current node.
I.e. either the explicitly specified next node or the node after the current node."
    ;; TODO
    )

(defun amigaguide-mode/goto-prev-node ()
    "Go to the previous node of the current node.
I.e. either the explicitly specified previous node or the node before the current node."
    ;; TODO
    )

(defun amigaguide-mode/get-node-ids ()
    (save-excursion
        (goto-char (point-min))

        (let (matches)
            (while (and (re-search-forward "@node +\\(.*\\)" nil t)
                        (not (eobp)))
                (push (match-string 1) matches))

            matches)))

(defun amigaguide-mode/hide-lines-starting-with (prefix)
    "Hide all lines that start with PREFIX."
    (interactive "sHide lines that start with prefix: " amigaguide-mode)

    (save-excursion
        (goto-char (point-min))

        (let ((inhibit-read-only t))

            (while (and (re-search-forward (concat "^" prefix) nil t)
                        (not (eobp)))
                (put-text-property (match-beginning 0) (+ 1  (match-end 0)) 'invisible t)))))

(defun amigaguide-mode/goto-node (node)
    (interactive
     (list (completing-read "sach an: " (amigaguide-mode/get-node-ids))))
    
    (alert (format "arg: %s, arg2: %s" arg arg2)))

(defun amigaguide-mode/display-node (node-id)
    "Display the node given by NODE-ID"
    (interactive "sNode ID: ")

    (goto-char (point-min))
    (let* ((found (re-search-forward (concat "^@node " node-id) nil t))
           (end (and found (re-search-forward "@endnode" nil t))))
        
        (alert (format "Found: %s, end: %s" found end))

        (when (and found end)
            (goto-char found)
            (beginning-of-line)
            (narrow-to-region (point) end))))

(defun amigaguide-mode/fontify ()
    (interactive)

    (save-excursion
        (let ((inhibit-read-only t)) 
            (goto-char (point-min))

            (amigaguide-mode/hide-lines-starting-with "@database\\|@index\\|@master\\|@title")
            (amigaguide-mode/hide-lines-starting-with "@remark")
            
            (while (and (re-search-forward "@{b}.*@{ub}" nil t)
                        (not (eobp)))
                (put-text-property (match-beginning 0) (match-end 0) 'font-lock-face 'bold))

            (goto-char (point-min))
            (while (and (re-search-forward "@{\\\"\\(.*?\\)\\\" *link *.+?}" nil t)
                        (not (eobp)))
                (put-text-property (match-beginning 0) (match-end 0) 'font-lock-face 'amigaguide-link)
                (put-text-property (match-beginning 0) (match-end 0) 'help-echo (concat "Go to node" (match-string 1)))))))

(easy-menu-define amigaguide-mode-menu amigaguide-mode-map
    "Menu for AmigaGuide files"
    '("AmigaGuide"
      ["Main" (amigaguide-mode/goto-node "Main")
       :help "gibt es heute nicht"]))

(defun amigaguide-mode/init-keymap ()
    (let ((map amigaguide-mode-map))
        (define-key map "i" 'amigaguide-mode/show-index)))

(define-derived-mode amigaguide-mode text-mode "AmigaGuide"
    "Major mode for browsing and editing AmigaGuide files"
    :group 'amigaguide
    
    (amigaguide-mode/init-keymap)

    (set (make-local-variable 'buffer-read-only) t)
    (set (make-local-variable 'imenu-generic-expression)
         '((nil "^@node\\s-+\\(.*\\)" 1)))

    (amigaguide-mode/fontify)

    (set-buffer-modified-p nil)
    (setq buffer-read-only t))

(provide 'amigaguide-mode)

;;; amigaguide-mode.el ends here
